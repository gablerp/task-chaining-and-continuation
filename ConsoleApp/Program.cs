﻿using ConsoleApp.Utilities;
using System;
using System.Threading.Tasks;

namespace ConsoleApp
{
    /// <summary>
    /// This class is a demo to show how methods form ArrayOperations class are chained and awaited at once.
    /// </summary>
    public static class Program
    {
        public static ArrayOperations ArrayOp { get; set; } = new ArrayOperations();
        public static async Task Main() => await Task.Run(() =>
        {
            return ArrayOp.GetArrayWithRandomNumbers();

        }).ContinueWith(
            x =>
            {
                if (x.Status == TaskStatus.RanToCompletion)
                {
                    Console.WriteLine("An array of intigers was genereted sucessfully:");
                    for (int i = 0; i < x.Result.Length; i++)
                    {
                        Console.WriteLine($"Array[{i}] = {x.Result[i]}");
                    }
                    int[] vs = x.Result;
                    try
                    {
                        return ArrayOp.MultiplyArrayByRandomNumber(vs);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Exception occuerd after trying to multiply an array by random number:");
                        Console.WriteLine(e.Message);
                    }    
                }
                return null;
            }).ContinueWith(
            y =>
            {
                if (y.Status == TaskStatus.RanToCompletion)
                {
                    Console.WriteLine("An array of intigers was multiplyed by a random number:");
                    for (int i = 0; i < y.Result.Length; i++)
                    {
                        Console.WriteLine($"Array[{i}] = {y.Result[i]}");
                    }
                    int[] ma = y.Result;
                    try
                    {
                        return ArrayOp.SortArrayAscending(ma);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Exception occuerd after trying to sort an array:");
                        Console.WriteLine(e.Message);
                    }
                    
                }
                return null;
            }).ContinueWith(
            z =>
            {
                if (z.Status == TaskStatus.RanToCompletion)
                {
                    Console.WriteLine("An array of intigers was sorted ascending:");
                    for (int i = 0; i < z.Result.Length; i++)
                    {
                        Console.WriteLine($"Array[{i}] = {z.Result[i]}");
                    }
                    int[] sa = z.Result;
                    try
                    {
                        return ArrayOp.GetAverageFormArray(sa);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Exception occuerd after trying calcualte an average:");
                        Console.WriteLine(e.Message);
                    }
                    
                }
                return 0;
            }).ContinueWith(
            avg =>
            {
                if (avg.Status == TaskStatus.RanToCompletion)
                {
                    Console.WriteLine("An average from all integers of an array:");
                    Console.WriteLine(avg.Result);
                    Console.ReadLine();
                }
                else
                {
                    Console.WriteLine("Task wasn't completed.");
                    Console.ReadLine();
                }
            });
    }
}
